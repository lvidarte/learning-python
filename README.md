# learning-python

Try it out on Binder! [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/lvidarte%2Flearning-python/master?filepath=Index.ipynb)

You can also get this tutorial and run it on your laptop:

## Clone the repo

    git clone https://gitlab.com/lvidarte/learning-python.git

## Create environment

    cd learning-python
    python3 -m venv env

## Activate environment

    source env/bin/activate

## Install packages

    pip install --upgrade pip
    pip install -r requirements.txt

## Run notebook

    jupyter notebook

