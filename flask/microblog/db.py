import sqlite3
import config


def get_connection():
    return sqlite3.connect(
        config.DB_FILEPATH,
        detect_types=sqlite3.PARSE_DECLTYPES
    )

def get_db():
    db = get_connection()
    db.row_factory = sqlite3.Row
    return db
